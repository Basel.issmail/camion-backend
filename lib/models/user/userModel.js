import Sequelize from 'sequelize';
import sequelize from '../../config/database';


const User = sequelize.define('user', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    },
  },
  telephone: {
    type: Sequelize.STRING,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  role: {
    type: Sequelize.ENUM,
    values: ['ADMIN', 'COMPANY', 'TRUCK'],
  },
  lat: {
    type: Sequelize.DOUBLE,
    validate: {
      isNumeric: true,
    },
  },
  lng: {
    type: Sequelize.DOUBLE,
    validate: {
      isNumeric: true,
    },
  },
  approved: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  deleted: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
});

export default User;
