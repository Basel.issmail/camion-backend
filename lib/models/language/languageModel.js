import Sequelize from 'sequelize';
import sequelize from '../../config/database';


const Language = sequelize.define('language', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  english: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  italian: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
});

export default Language;
