import User from './user/userModel';
import Company from './company/companyModel';
import ServiceType from './service-type/serviceTypeModel';
import CarBrand from './car-brand/carBrandModel';
import Language from './language/languageModel';
import Truck from './truck/truckModel';
import Attachment from './attachment/attachmentModel';
import sequelize from '../config/database';
import config from '../config/config';

const models = {
  User,
  Company,
  ServiceType,
  CarBrand,
  Language,
  Attachment,
  Truck,
  initiateAssociations() {
    // User may be a company
    this.Company.User = Company.belongsTo(User);

    // attachment may be in a company
    this.Company.Attachment = Company.belongsTo(Attachment);

    // company has service types
    this.Company.ServiceType = Company.belongsTo(ServiceType);

    // company works with car brands
    this.Company.CarBrand = Company.belongsTo(CarBrand);

    // company speaks languages
    this.Company.Language = Company.belongsTo(Language);


    // User may be a truck
    this.Truck.User = Truck.belongsTo(User);

    // vehicleImage may be in a truck
    this.Truck.Attachment = Truck.belongsTo(Attachment, { as: 'vehicleImage' });

    // bookletImage may be in a truck
    this.Truck.Attachment = Truck.belongsTo(Attachment, { as: 'bookletImage' });

    // licenseImage may be in a truck
    this.Truck.Attachment = Truck.belongsTo(Attachment, { as: 'licenseImage' });

    // Drop and Recreate tables
    if (config.db.sync) {
      sequelize.sync();
    }
  },
};


export default models;
