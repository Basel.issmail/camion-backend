import Sequelize from 'sequelize';
import sequelize from '../../config/database';


const ServiceType = sequelize.define('serviceType', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  rescue: {
    type: Sequelize.ENUM,
    values: ['NO', 'ALLOW_HIGHWAY', 'NO_HIGHWAY'],
  },
  tireRepair: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  mobileWorkshop: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  mctc: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  windshieldRepair: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  dtco: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  vehicleWash: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  semiTrailerRepair: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  tarpaulinReplacementRepair: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  generatorReplacementRepair: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  towingCranes: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
});

export default ServiceType;
