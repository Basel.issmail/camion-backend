import Sequelize from 'sequelize';
import sequelize from '../../config/database';


const Attachment = sequelize.define('attachment', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  mimetype: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  tag: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  path: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  size: {
    type: Sequelize.BIGINT,
    allowNull: false,
  },
});

export default Attachment;
