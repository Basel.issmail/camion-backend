import Sequelize from 'sequelize';
import sequelize from '../../config/database';


const CarBrand = sequelize.define('carBrand', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  iveco: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  scania: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  daf: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  volvo: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  mercedes: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  renault: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  isuzu: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  evobus: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
});

export default CarBrand;
