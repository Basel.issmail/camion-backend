import Sequelize from 'sequelize';
import sequelize from '../../config/database';
import models from '../models';

const Truck = sequelize.define('truck', {
  id: {
    type: Sequelize.BIGINT,
    primaryKey: true,
    autoIncrement: true,
  },
  nickname: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      is: /^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i,
      // isUnique(value, next) {
      //   models.Truck.find({
      //     where: { nickname: value },
      //     attributes: ['id'],
      //   }).done((truck) => {
      //     if (truck) { return next('nickname must be unique'); }

      //     next();
      //   });
      // },
    },
  },
  firstName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  lastName: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  companyName: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  vat: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  fiscalCode: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  carBrand: {
    type: Sequelize.ENUM,
    values: ['IVECO', 'SCANIA', 'DAF', 'VOLVO', 'MERCEDES', 'RENAULT', 'ISUZU', 'EVOBUS'],
    allowNull: false,
  },
  preferredPayment: {
    type: Sequelize.ENUM,
    values: ['CREDIT_CARD'],
    allowNull: false,
  },
});

export default Truck;
