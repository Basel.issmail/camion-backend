import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import { urlencoded, json } from 'body-parser';
import expressValidator from 'express-validator';
// setup global middleware here

export default function (app) {
  app.use(cors({
    allowedHeaders: ['sessionId', 'Content-Type', 'Authorization'],
    exposedHeaders: ['sessionId'],
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
  }));
  app.use(morgan('dev'));
  app.use('/uploads', express.static('uploads'));
  app.use('/', express.static('camion', {
    extensions: ['html'],
  }));
  app.use(urlencoded({ extended: true }));
  app.use(expressValidator());
  app.use(json());
}
