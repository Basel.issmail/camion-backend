const multer = require('multer');

export default function (savePath) {
  const storage = multer.diskStorage({
    destination(req, file, cb) {
      cb(null, `./uploads/${savePath}`);
    },
    filename(req, file, cb) {
      cb(null, Date.now() + file.originalname.replace(/\s+/g, '_'));
    },
  });

  const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      req.fileValidationError = {
        code: 'FORBIDDEN_EXTENSION',
        message: 'Forbidden extension',
        name: 'MulterError',
        storageErrors: [],
      };
      cb(new Error('goes wrong on the mimetype'), false);
    }
  };

  const upload = multer({
    storage,
    limits: {
      fileSize: 1024 * 1024 * 10,
    },
    fileFilter,
  });

  return upload;
}
