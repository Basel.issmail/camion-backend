import { verify } from 'jsonwebtoken';
import config from '../config/config';

function verifyToken(expectedRoles) {
  return (req, res, next) => {
    const camionHeaderParts = req.headers.authorization.split(' ');
    const prefix = camionHeaderParts[0];
    const token = camionHeaderParts[1];
    if (prefix !== 'Camion' || !token) {
      return res.sendStatus(403);
    }
    let decoded = '';
    try {
      decoded = verify(
        token,
        config.secret,
      );
    } catch (error) {
      return res.sendStatus(403);
    }

    const roles = decoded.user.role;
    if (!roles) {
      return res.sendStatus(403);
    }
    if (roles && expectedRoles.some(scope => roles.indexOf(scope) !== -1)) {
      next();
    } else {
      res.sendStatus(403);
    }
  };
}

export default verifyToken;
