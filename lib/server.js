import express from 'express';
import path from 'path';
import sequelize from './config/database';
import logger from './util/logger';
import api from './api/api';
import verifyToken from './middleware/auth';
import appMiddlware from './middleware/appMiddlware';
import models from './models/models';

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');


// const config = require('./config/config');

const app = express();
// Sync Database
models.initiateAssociations();

// Testing Database connection
sequelize
  .authenticate()
  .then(() => {
    logger.log('Connection has been established successfully.');
  })
  .catch((err) => {
    logger.log('Unable to connect to the database:', err);
  });

// setup the app middlware
appMiddlware(app);

// setup the api
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/v1', api);

// set up global error handling
app.use((req, res, next) => {
  res.status(404);

  res.format({
    html() {
      res.sendFile(path.join(`${__dirname}/../camion/404.html`));
    },
    json() {
      res.json({ error: 'Not found' });
    },
    default() {
      res.type('txt').send('Not found');
    },
  });
});

app.use((err, req, res, next) => {
  if (err instanceof SyntaxError) return res.status(400).json({ error: 'Invalid JSON' });
  if (err instanceof TypeError) return res.status(500).json({ error: 'Missing Properties' });
  if (err.message) return res.status(400).send({ error: err.message });
  res.status(500).send(err);
});

// export the app for testing
module.exports = app;
