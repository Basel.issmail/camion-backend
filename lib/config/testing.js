module.exports = {
  // enabled logging for development
  logging: true,
  secret: 'SeCrEt',
  db: {
    sync: true,
    database: 'camion_dev',
    username: 'root',
    password: 'asdfgh',
    host: 'localhost',
    logging: console.log,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
};
