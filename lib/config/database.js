import Sequelize from 'sequelize';
import config from './config';

// db.url is different depending on NODE_ENV
const sequelize = new Sequelize(config.db.database, config.db.username, config.db.password, {
  host: config.db.host,
  dialect: 'mysql',
  operatorsAliases: false,
  logging: config.db.logging,
  typeValidation: true,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});
export default sequelize;
