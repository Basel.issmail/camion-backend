const router = require('express').Router();

// api router will mount other routers
// for all our resources
router.use('/user', require('./user/userRoutes').default);
router.use('/company', require('./company/companyRoutes').default);
router.use('/truck', require('./truck/truckRoutes').default);
router.use('/attachment', require('./attachment/attachmentRoutes').default);

export default router;
