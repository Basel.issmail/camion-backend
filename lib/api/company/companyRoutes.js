import Sequelize from 'sequelize';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import models from '../../models/models';
import verifyToken from '../../middleware/auth';
import config from '../../config/config';
import sequelize from '../../config/database';

const { Op } = Sequelize;

const router = require('express').Router();

router.route('/register')
  .post(async (req, res, next) => {
    try {
      if (!req.body.user) throw new Error('user Object is required');
      if (!req.body.user.password) throw new Error('password is required');
      if (!req.body.user.email) throw new Error('email is required');
      if (!req.body.serviceType) req.body.serviceType = {};
      if (!req.body.carBrand) req.body.carBrand = {};
      if (!req.body.language) req.body.language = {};

      const company = req.body;
      company.user.role = 'COMPANY';
      company.user.approved = false;
      company.user.deleted = false;
      company.user.serviceType = null;
      company.user.carBrand = null;
      company.user.language = null;

      const hashCost = 10;
      company.user.password = await bcrypt.hash(req.body.user.password, hashCost);
      sequelize.transaction({ autocommit: false }).then(transaction => models.Company.create(company, {
        transaction,
        include: [{
          association: models.Company.User,
        },
        {
          association: models.Company.ServiceType,
        }, {
          association: models.Company.CarBrand,
        }, {
          association: models.Company.Language,
        },
        ],
      }).then((response) => {
        transaction.commit();
        res.json({ message: 'Company Registered Successfully' });
      }).catch((err) => {
        transaction.rollback();
        res.status(400).json(err);
      }));
      // }
    } catch (error) {
      next(error);
    }
  });

router.route('/login')
  .post((req, res, next) => {
    models.Company.findOne({
      attributes: { exclude: ['attachmentId'] },
      include: [{
        association: models.Company.User,
        attributes: { exclude: ['id', 'deleted'] },
        where: {
          email: req.body.email,
          deleted: false,
        },
      },
      {
        association: models.Company.ServiceType,
      }, {
        association: models.Company.CarBrand,
      }, {
        association: models.Company.Language,
      }, {
        association: models.Company.Attachment,
      },
      ],
    }).then(async (company) => {
      if (!company || company.user.role !== 'COMPANY') {
        return res.status(401).json({
          message: 'Username or password are incorrect',
          code: 'WRONG_USERNAME_OR_PASSWORD',
        });
      }
      const valid = await bcrypt.compare(req.body.password, company.user.password);
      if (!valid) {
        return res.status(401).json({
          message: 'Username or password are incorrect',
          code: 'WRONG_USERNAME_OR_PASSWORD',
        });
      }

      const token = jwt.sign(
        {
          user: {
            role: company.user.role,
          },
        },
        config.secret,
        {
          expiresIn: '1y',
        },
      );
      company.user.password = undefined;
      res.json({ token, company });
    }).catch((err) => {
      console.log(err);
      res.status(400).json(err);
    });
  });

router.post('/list', verifyToken(['COMPANY', 'ADMIN', 'TRUCK']), (req, res) => {
  let { limit, offset } = req.query;
  limit = (Number(req.query.limit) && req.query.limit < 50) ? Number(req.query.limit) : 50;
  offset = Number(req.query.offset) || 0;
  const sortDirection = (req.query.order) ? req.query.order.trim().toUpperCase() : 'ASC';
  console.log(req.body.email);
  models.Company.findAndCountAll({
    order: [
      ['createdAT', sortDirection],
    ],
    limit,
    offset,
    where: {
      name: {
        [Op.like]: [`%${req.body.name ? req.body.name : ''}%`],
      },
    },
    include: [{
      model: models.User,
      attributes: { exclude: ['id', 'password', 'deleted'] },
      where: {
        email: {
          [Op.like]: [`%${req.body.email ? req.body.email : ''}%`],
        },
      },
    },
    {
      model: models.ServiceType,
      attributes: { exclude: ['id'] },
    }, {
      model: models.CarBrand,
      attributes: { exclude: ['id'] },
    }, {
      model: models.Language,
      attributes: { exclude: ['id'] },
    },
    ],
  }).then(async (company) => {
    res.json(company);
  });
});

export default router;
