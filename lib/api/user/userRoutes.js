import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import models from '../../models/models';
import config from '../../config/config';

const router = require('express').Router();

// setup boilerplate route jsut to satisfy a request
// for building

router.route('/login')
  .post((req, res, next) => {
    models.User.findOne({ where: { email: req.body.email } }).then(async (user) => {
      if (!user || user.role !== 'ADMIN') {
        return res.status(401).json({
          message: 'Username or password are incorrect',
          code: 'WRONG_USERNAME_OR_PASSWORD',
        });
      }
      const valid = await bcrypt.compare(req.body.password, user.password);
      if (!valid) {
        return res.status(401).json({
          message: 'Username or password are incorrect',
          code: 'WRONG_USERNAME_OR_PASSWORD',
        });
      }

      const token = jwt.sign(
        {
          user: {
            role: user.role,
          },
        },
        config.secret,
        {
          expiresIn: '1y',
        },
      );

      res.json({ token, expiresIn: 360 });
    });
  });

// router.route('/usernameAvailable')
//   .post((req, res) => {
//     const { username } = req.body;
//     if (!username || username.length < 4) {
//       return res.json({
//         usernameAvailable: false,
//         message: 'username should be more than 4 characters',
//       });
//     }
//     if (/^[a-z\d](?:[a-z\d]|-(?=[a-z\d])){0,38}$/i.test(username) === false) {
//       return res.json({
//         usernameAvailable: false,
//         message: `Ogni parte del nick deve avere una lunghezza non superiore a 32.
//         Ogni parte del nick deve iniziare e terminare con un alfanumerico (cioè lettere [A-Za-z] o cifre [0-9]).
//         Ogni parte del nick può contenere trattino, ma non può iniziare o terminare con un trattino.`,
//       });
//     }

//     models.User.count({
//       where: {
//         username,
//       },
//     }).then((result) => {
//       if (result > 0) {
//         res.json({
//           usernameAvailable: false,
//           message: 'username not available',
//         });
//       } else {
//         res.json({
//           usernameAvailable: true,
//           message: 'username available',
//         });
//       }
//     });
//   });

router.route('/email')
  .post((req, res) => {
    const { email } = req.body;
    if (!email) {
      return res.json({
        emailUnique: false,
        message: 'campo necessario',
        code: 'EMAIL_REQUIRED',
      });
    }
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(String(email).toLowerCase()) === false) {
      return res.json({
        emailUnique: false,
        message: 'Email invalida',
        code: 'EMAIL_NOT_VALID',
      });
    }
    models.User.count({
      where: {
        email,
      },
    }).then((result) => {
      if (result > 0) {
        res.json({
          emailUnique: false,
          message: 'email not available',
          code: 'EMAIL_NOT_AVAILABLE',
        });
      } else {
        res.json({
          emailUnique: true,
          message: 'email is available',
          code: 'EMAIL_AVAILABLE',
        });
      }
    });
  });

export default router;
