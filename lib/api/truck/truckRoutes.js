import Sequelize from 'sequelize';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import models from '../../models/models';
import verifyToken from '../../middleware/auth';
import config from '../../config/config';
import sequelize from '../../config/database';


const router = require('express').Router();


router.route('/register')
  .post(async (req, res, next) => {
    try {
      if (!req.body.user) throw new Error('user Object is required');
      if (!req.body.user.password) throw new Error('password is required');
      if (!req.body.user.email) throw new Error('email is required');

      const Truck = req.body;
      Truck.user.role = 'TRUCK';
      Truck.user.approved = false;
      Truck.user.deleted = false;

      const hashCost = 10;
      Truck.user.password = await bcrypt.hash(req.body.user.password, hashCost);
      sequelize.transaction({ autocommit: false }).then(transaction => models.Truck.create(Truck, {
        transaction,
        include: [{
          association: models.Truck.User,
        }],
      }).then((response) => {
        transaction.commit();
        res.json({ message: 'User Registered Successfully' });
      }).catch((err) => {
        transaction.rollback();
        res.status(400).json(err);
      }));
    } catch (error) {
      next(error);
    }
  });


router.route('/login')
  .post((req, res, next) => {
    models.Truck.findOne({
      attributes: { exclude: ['vehicleImageId', 'bookletImageId', 'licenseImageId'] },
      include: [{
        model: models.User,
        attributes: { exclude: ['id', 'deleted'] },
        where: {
          email: req.body.email,
          deleted: false,
        },
      },
      {
        model: models.Attachment,
        as: 'vehicleImage',
      },
      {
        model: models.Attachment,
        as: 'bookletImage',
      },
      {
        model: models.Attachment,
        as: 'licenseImage',
      },
      ],
    }).then(async (truck) => {
      if (!truck || truck.user.role !== 'TRUCK') {
        return res.status(401).json({
          message: 'Username or password are incorrect',
          code: 'WRONG_USERNAME_OR_PASSWORD',
        });
      }
      const valid = await bcrypt.compare(req.body.password, truck.user.password);
      if (!valid) {
        return res.status(401).json({
          message: 'Username or password are incorrect',
          code: 'WRONG_USERNAME_OR_PASSWORD',
        });
      }

      const token = jwt.sign(
        {
          user: {
            role: truck.user.role,
          },
        },
        config.secret,
        {
          expiresIn: '1y',
        },
      );
      truck.user.password = undefined;
      res.json({ token, truck });
    });
  });

router.post('/list', verifyToken(['COMPANY', 'ADMIN', 'TRUCK']), (req, res) => {
  let { limit, offset } = req.query;
  limit = (Number(req.query.limit) && req.query.limit < 50) ? Number(req.query.limit) : 50;
  offset = Number(req.query.offset) || 0;
  const sortDirection = (req.query.order) ? req.query.order.trim().toUpperCase() : 'ASC';
  models.Truck.findAndCountAll({
    order: [
      ['createdAT', sortDirection],
    ],
    limit,
    offset,
    include: [{
      model: models.User,
      attributes: { exclude: ['id', 'password', 'deleted'] },
    },
    ],
  }).then(async (truck) => {
    res.json(truck);
  });
});

export default router;
