import fileUploader from '../../middleware/imageUploader';
import Attachment from '../../models/attachment/attachmentModel';

const router = require('express').Router();

const upload = fileUploader('company');

router.post('/upload', upload.single('attachment'), (req, res) => {
  if (req.fileValidationError) {
    return res.status(500).send(req.fileValidationError);
  }
  // console.log(req.file);
  Attachment.create({
    name: req.file.filename, mimetype: req.file.mimetype, path: req.file.path, size: req.file.size, tag: 'test',
  })
    .then((response) => {
      res.json(response);
    });
});

export default router;
