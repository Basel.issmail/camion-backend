// intro point for our server.
// PRO-TIP: if you have an index.js file
// on the root of a folder in node
// you can just require that folder and node will
// automatically require the index.js on the root

// setup config first before anything by requiring it
import { port } from './config/config';
import app from './server';
import { log } from './util/logger';

app.listen(port);
log(`listening on http://localhost:${port}`);
